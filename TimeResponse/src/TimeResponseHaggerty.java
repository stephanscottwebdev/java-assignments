import java.time.LocalDateTime;

import javax.swing.JOptionPane;

public class TimeResponseHaggerty {
	// Kathy Haggerty
	public static void main(String[] args) {
		LocalDateTime time1 = LocalDateTime.now();
		int secs1 = time1.getSecond();
		int min1 = time1.getMinute();

		JOptionPane.showConfirmDialog(null, "Are you a closet Taylor Swift fan?");

		LocalDateTime time2 = LocalDateTime.now();
		int secs2 = time2.getSecond();
		int min2 = time2.getMinute();
		int difference = 0;
		if (min1 == min2){
			 difference = secs2 - secs1;
		}else{
			 difference = (min2 * 60 + secs2) - (min1*60 + secs1);
		}
		

		JOptionPane.showMessageDialog(null, "End seconds: " + secs2 + "\nStart seconds: " + secs1 + "\nIt took "
				+ difference + " seconds for you to answer.\nWas it a difficult decision?");

	}

}
